```python
import matplotlib
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

## More complex systems

!!! summary "Learning goals"

    After this lecture you will be able to:

    - formulate equations of motion for electrons or phonons in 1D, with multiple degrees of freedom per unit cell.
    - solve these equations to arrive at the dispersion relation.
    - derive the group velocity, effective mass, and density of states.
    - explain what happens with the band structure when the periodicity of the lattice is increased or reduced.

### More degrees of freedom per unit cell:

![](figures/phonons5.svg)

Before we used that all atoms are similar, but this doesn't work anymore. We need to generalize our anzatz. We label all degrees of freedom in each **unit cell** (a repeated element of the system).

For two atoms in a unit cell we have displacements $u_{1,n}$ and $u_{2,n}$. Here the first index is the atom number within the unit cell, second is the unit cell number. The atom with mass $M$ is the atom 1, and the atom with mass $m$ is the atom 2.

Having the degrees of freedom let's write down the equations of motion:

$$
\begin{aligned}
M\ddot{u}_{1,n}=\kappa(u_{2,n}-2u_{1,n}+u_{2,n-1})\\
m\ddot{u}_{2,n}=\kappa(u_{1, n} - 2u_{2,n}+u_{1,n+1}),
\end{aligned}
$$

The new Ansatz is conceptually the same as before: all unit cells should be the same up to a phase factor:

$$
\begin{pmatrix}
u_{1,n}\\
u_{2,n}
\end{pmatrix} =
e^{i\omega t - ik na}
\begin{pmatrix}
u_{1}\\
u_{2}
\end{pmatrix}.
$$

Substituting this ansatz into the equations of motion we end up with an eigenvalue problem:
$$\omega^2
\begin{pmatrix}
M & 0 \\ 0 & m
\end{pmatrix}
\begin{pmatrix}
u_{1} \\ u_{2}
\end{pmatrix} = \kappa
\begin{pmatrix}
2 & -1 - e^{ika} \\ -1-e^{-ika} & 2
\end{pmatrix}
\begin{pmatrix}
u_{1}\\ u_{2}
\end{pmatrix},$$
with eigenfrequencies
$$\omega^2=\frac{\kappa(M+m)}{Mm}\pm \kappa\left\{\left(\frac{M+m}{Mm}\right)^2-\frac{4}{Mm}{\rm sin}^2\left(\frac{1}{2}ka\right)\right\}^{\frac{1}{2}}$$

Looking at the eigenvectors we see that for every $k$ there are now two values of $\omega$: one corresponding to in-phase motion (–) and anti-phase (+).

```python
def dispersion_2m(k, kappa=1, M=1.4, m=1, acoustic=True):
    Mm = M*m
    m_harm = (M + m) / Mm
    root = kappa * np.sqrt(m_harm**2 - 4*np.sin(k/2)**2 / Mm)
    if acoustic:
        root *= -1
    return np.sqrt(kappa*m_harm + root)

# TODO: Add panels with eigenvectors
k = np.linspace(-2*pi, 6*pi, 300)
fig, ax = pyplot.subplots()
ax.plot(k, dispersion_2m(k, acoustic=False), label='optical')
ax.plot(k, dispersion_2m(k), label='acoustic')
ax.set_xlabel('$ka$')
ax.set_ylabel(r'$\omega$')
pyplot.xticks([-pi, 0, pi], [r'$-\pi$', '$0$', r'$\pi$'])
pyplot.yticks([], [])
pyplot.vlines([-pi, pi], 0, 2.2, linestyles='dashed')
pyplot.legend()
pyplot.xlim(-1.75*pi, 3.5*pi)
pyplot.ylim(bottom=0)
draw_classic_axes(ax, xlabeloffset=.2)
```

The lower branch is called _acoustic_ because its linear dispersion near $\omega=0$ matches the behavior of the regular sound waves.
The upper branch is the _optical branch_ because the high phonon frequency allows them to efficiently emit and adsorb photons.

The _density of states_ (DOS) is given by $g(\omega)=\rho_{\rm S}\frac{ {\rm d}k}{ {\rm d}\omega}$. 

We can derive $\frac{ {\rm d}k}{ {\rm d}\omega}$ from the dispersion relation $\omega(k)$, as graphically shown here:

```python
matplotlib.rcParams['font.size'] = 24
k = np.linspace(-.25*pi, 1.5*pi, 300)
k_dos = np.linspace(0, pi, 20)
fig, (ax, ax2) = pyplot.subplots(ncols=2, sharey=True, figsize=(10, 5))
ax.plot(k, dispersion_2m(k, acoustic=False), label='optical')
ax.plot(k, dispersion_2m(k), label='acoustic')
ax.vlines(k_dos, 0, dispersion_2m(k_dos, acoustic=False),
          colors=(0.5, 0.5, 0.5, 0.5))
ax.hlines(
    np.hstack((dispersion_2m(k_dos, acoustic=False), dispersion_2m(k_dos))),
    np.hstack((k_dos, k_dos)),
    1.8*pi,
    colors=(0.5, 0.5, 0.5, 0.5)
)
ax.set_xlabel('$ka$')
ax.set_ylabel(r'$\omega$')
ax.set_xticks([0, pi])
ax.set_xticklabels(['$0$', r'$\pi$'])
ax.set_yticks([], [])
ax.set_xlim(-pi/4, 2*pi)
ax.set_ylim((0, dispersion_2m(0, acoustic=False) + .2))
draw_classic_axes(ax, xlabeloffset=.2)

k = np.linspace(0, pi, 1000)
omegas = np.hstack((
    dispersion_2m(k, acoustic=False), dispersion_2m(k)
))
ax2.hist(omegas, orientation='horizontal', bins=75)
ax2.set_xlabel(r'$g(\omega)$')
ax2.set_ylabel(r'$\omega$')

# Truncate the singularity in the DOS
max_x = ax2.get_xlim()[1]
ax2.set_xlim((0, max_x/2))
draw_classic_axes(ax2, xlabeloffset=.1)
matplotlib.rcParams['font.size'] = 16
```

Note that $g(\omega)$ is generally plotted along the vertical axis and $\omega$ along the horizontal axis – the plot above is just to demonstrate the relation between the dispersion and the DOS. The singularities in $g(\omega)$ at the bottom and top of each branch are called _van Hove singularities_.

### Consistency check with 1 atom per cell

But what if we take $M\rightarrow m$? Then we should reproduce the previous result with only one band!

The reason why we get two bands is because we have $a\rightarrow 2a$ compared to 1 atom per unit cell!

For $M\rightarrow m$, the dispersion diagram should come back to the diagram obtained for the first example (i.e. with identical masses).

To reconcile the two pictures, let's plot two unit cells in reciprocal space. (Definition: each of these unit cells is called a **Brillouin zone**, a concept that will come up later.)

```python
k = np.linspace(0, 2*pi, 300)
k_dos = np.linspace(0, pi, 20)
fig, ax = pyplot.subplots()
ax.plot(k, dispersion_2m(k, acoustic=False), label='optical')
ax.plot(k, dispersion_2m(k), label='acoustic')
omega_max = dispersion_2m(0, acoustic=False)
ax.plot(k, omega_max * np.sin(k/4), label='equal masses')
ax.set_xlabel('$ka$')
ax.set_ylabel(r'$\omega$')
ax.set_xticks([0, pi, 2*pi])
ax.set_xticklabels(['$0$', r'$\pi/2a$', r'$\pi/a$'])
ax.set_yticks([], [])
ax.set_xlim(-pi/8, 2*pi+.4)
ax.set_ylim((0, dispersion_2m(0, acoustic=False) + .2))
ax.legend(loc='lower right')
pyplot.vlines([pi, 2*pi], 0, 2.2, linestyles='dashed')
draw_classic_axes(ax, xlabeloffset=.2)
```

We see that doubling the lattice constant "folds" the band structure on itself, doubling all the bands.

### Total number of states

Which values of $k$ are allowed for a finite 1D crystal of length $L$?

Assume closed boundary conditions $\rightarrow$ standing waves $\rightarrow k=\frac{\pi}{L},\frac{2\pi}{L},\frac{3\pi}{L},\frac{4\pi}{L}...$

$\rightarrow \rho_{\rm S}(k)=$ (no. of $k$-values / unit of $k$) $=\frac{L}{\pi}$, where only the positive part of $k$-space is filled with allowed $k$-values.

For open boundary conditions $\rightarrow$ running waves $\rightarrow k=$ $...\frac{-4\pi}{L},\frac{-2\pi}{L},0,\frac{2\pi}{L},\frac{4\pi}{L}...$

$\rightarrow \rho_{\rm R}(k)=\frac{L}{2\pi}$, which is lower than for the case of closed boundary conditions, however, in this case the entire $k$-space is filled.

## Summary

* By using plane waves in real space as an Ansatz, we found all normal modes and eigenvectors. (Just like in the case of 1 degree of freedom per unit cell).
* The dispersion relation of a system with period $a$ in real space is periodic with period $2\pi/a$ in $k$-space
* In a system with more than one degree of freedom per unit cell we need to consider independent amplitudes for each degree of freedom, and get multiple bands.

## Exercises
#### Exercise 1: analyzing the diatomic vibrating chain
Recall the eigenfrequencies of a diatomic vibrating chain in the lecture notes with 2 different masses (can be found below [here](#more-degrees-of-freedom-per-unit-cell)).

1. Find the magnitude of the group velocity near $k=0$ for the _acoustic_ branch.

    ??? hint
        Make use of a Taylor expansion.

2. Show that the group velocity at $k=0$ for the _optical_ branch is zero.
3. Derive an expression for the density of states $g(\omega)$ for the _acoustic_ branch and small $ka$. Make use of your expression for the group velocity in 1.

#### Exercise 2: atomic chain with 3 different spring constants
Suppose we have a vibrating 1D atomic chain with 3 different spring constants alternating like $\kappa_ 1$, $\kappa_2$, $\kappa_3$, $\kappa_1$, etc. All the the atoms in the chain have an equal mass $m$.

1. Make a sketch of this chain and indicate the length of the unit cell $a$ in this sketch.
2. Derive the equations of motion for this chain.
3. By filling in the trial solutions for the equations of motion (which should be similar to the ones in eqs. (10.3) and (10.4) of the book), show that the eigenvalue problem is given by $$ \omega^2 \mathbf{x} = \frac{1}{m} \begin{pmatrix} \kappa_1 + \kappa_ 3 &  -\kappa_ 1 & -\kappa_ 3 e^{i k a} \\ -\kappa_ 1 & \kappa_1+\kappa_2 & -\kappa_ 2 \\  -\kappa_ 3 e^{-i k a} & -\kappa_2 & \kappa_2 + \kappa_ 3 \end{pmatrix} \mathbf{x}$$
4. In general, the eigenvalue problem above cannot be solved analytically, and can only be solved in specific cases. Find the eigenvalues $\omega^2$ when $k a = \pi$ and $\kappa_ 1 = \kappa_ 2 = q$.

    ??? hint
    
        To solve the eigenvalue problem quickly, make use of the fact that the Hamiltonian in that case commutes with the matrix $$ X = \begin{pmatrix} 0 & 0 & 1 \\ 0 & 1 & 0 \\ 1 & 0 & 0 \end{pmatrix}. $$ What can be said about eigenvectors of two matrices that commute?

5. What will happen to the periodicity of the band structure if $\kappa_ 1 = \kappa_ 2 = \kappa_3$?

#### Exercise 3: the Peierls transition
In the previous lecture, we have derived the electronic band structure of an 1D, equally spaced atomic chain. Such chains, however, are in fact not stable and the equal spacing will be distorted. This is also known as the [Peierls transition](https://en.wikipedia.org/wiki/Peierls_transition).

The spacing of the distorted chain alternates between two different distances and this also causes the hopping energy to alternate between $t_1$ and $t_2$. We further set the onsite energies of the atoms to $\epsilon$. The situation is depicted in the figure below.

![](figures/peierls_transition.svg)

Due to the alternating hopping energies, we must treat two consecutive atoms as different orbitals ($\left| \phi_n \right>$ and $\left| \psi_n \right>$ in the figure) from the same unit cell. This also means that we expect to find two eigenvalues for the Schrödinger equation and we also need two different LCAO's to form a basis for the Hilbert space. These LCAO's are $$\left| \Phi\right>= \sum_n a_n \left| \phi_n \right>,$$ $$\left| \Psi\right>= \sum_n b_n \left| \psi_n \right>,$$ and the coefficients $a_n$ and $b_n$ are both given by $$a_n = b_n=\frac{e^{-ikna}}{\sqrt{N}},$$ where $N$ is the total number of unit cells in the (periodic) chain.

1. Indicate the length of the unit cell $a$ in the figure.
2. What are the values of the matrix elements $\left< \phi_n \right| H \left| \phi_{n} \right>$, $\left< \psi_n \right| H \left| \psi_{n} \right>$, $\left< \psi_n \right| H \left| \phi_{n} \right>$, and $\left< \psi_n \right| H \left| \phi_{n+1} \right>$?
3. Using the $\{ \left| \Phi\right>, \left| \Psi\right> \}$-basis, show that the Hamiltonian is given by $$H = \begin{pmatrix} \epsilon & t_1 + t_2 e^{i k a} \\  t_1 + t_2 e^{-i k a}  & \epsilon \end{pmatrix}.$$
4. Derive the dispersion relation of this Hamiltonian. Does it look like the figure of the band structure shown on the [Wikipedia page](https://en.wikipedia.org/wiki/Peierls_transition#/media/File:Peierls_instability_after.jpg)? Does it reduce to the 1D, equally spaced atomic chain if $t_1 = t_2$?
5. Find an expression of the group velocity $v(k)$ and effective mass $m^*(k)$ of both bands.
6. Derive an expression for the density of states $g(E)$ of the entire band structure and make a plot of it. Does your result makes sense when considering the band structure?
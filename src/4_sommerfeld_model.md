```python
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```
_(based on chapter 4 of the book)_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - calculate the electron density of states in 1D, 2D, and 3D using the Sommerfeld free-electron model.
    - express the number and energy of electrons in a system in terms of integrals over k-space.
    - use the Fermi distribution to extend the previous learning goal to finite T.
    - calculate the electron contribution to the specific heat of a solid.
    - describe central terms such as the Fermi energy, Fermi temperature, and Fermi wavevector.

## Quantization of k-space and density of states in the free electron model

Atoms in a metal provide conduction electrons from their outer shells (often s-shells). These electrons can be described as waves, analogous to phonons. The Hamiltonian of a free electron is:

$$
\mathcal{H}=\frac{ {\bf p}^2}{2m}=-\frac{\hbar^2}{2m}\left( \frac{\partial^2}{\partial x^2}+\frac{\partial^2}{\partial y^2}+\frac{\partial^2}{\partial z^2} \right)\ \Rightarrow\ \varepsilon=\frac{\hbar^2}{2m}\left( k_x^2+k_y^2+k_z^2 \right)
$$

If we impose periodic boundary conditions, $\psi(x,y,z)=\psi(x+l,y+L,z+L)$, it follows that the allowed wavenumbers are given by

$$
k_x=\frac{2\pi p}{L},\ k_y=\frac{2\pi q}{L},\ k_z=\frac{2\pi r}{L}\ \Rightarrow\ \varepsilon=\frac{2\pi^2\hbar^2}{mL^2}\left( p^2+q^2+r^2 \right).
$$
(this quantization of k-space is exactly analogous to that for phonons, see lecture 2 - Debye model)

```python
kf = 3;
extrapol = 1.1;
ks = np.arange(-kf, kf+1);
kcont = np.linspace(-extrapol*kf, extrapol*kf, 200);

Edis = ks**2;
Econt = kcont**2;

fig = pyplot.figure();
#fig.set_size_inches(2, 2)
ax = fig.add_subplot(111);
ax.plot(kcont, Econt);
ax.plot(ks, Edis, 'k.', markersize=10);
for i in range(2*kf + 1):
    ax.plot([ks[i], ks[i]], [0.0, Edis[i]], 'k:');
ax.set_xlim(-3.75, 3.75);
ax.set_ylim(0.0, 11);

ax.set_xlabel(r"$k \enspace \left[ \frac{2 \pi}{L} \right]$");
ax.set_ylabel(r"$\varepsilon$");

ax.set_xticklabels([""] + ks.tolist() + [""]);
ax.set_yticks([]);

draw_classic_axes(ax, xlabeloffset=.6);
```

The difference with phonons is that electrons are _fermions_, implying that there are only 2 electron states (due to spin) per $k$-value.

Given the number of electrons in a system, we can now fill up these states starting from the lowest energy until we run out of electrons, at which point we reach the _Fermi energy_.

![](figures/fermi_circle_periodic.svg)

Our goal now is to compute the density of states. We start by expressing the total number of states $N$ as an integral over k-space. Assuming three dimensions and spherical symmetry (the dispersion in the free electron model is isotropic), we find 

$$
N=2\left(\frac{L}{2\pi}\right)^3\int{\rm d}{\bf k}=2 \left(\frac{L}{2\pi}\right)^34\pi\int k^2{\rm d}k=\frac{V}{\pi^2}\int k^2{\rm d}k ,
$$

where the factor 2 represents spin degeneracy, and $\left(\frac{L}{2\pi}\right)^3$ is the density of points in k-space. 

Using $k=\frac{\sqrt{2m\varepsilon}}{\hbar}$ and ${\rm d}k=\frac{1}{\hbar}\sqrt{\frac{m}{2\varepsilon}}{\rm d}\varepsilon$ we can rewrite this as:

$$
N=\frac{V}{\pi^2}\int\frac{2m\varepsilon}{\hbar^3}\sqrt{\frac{m}{2\varepsilon}}{\rm d}\varepsilon=\frac{Vm^{3/2}}{\pi^2\hbar^3}\int\sqrt{2\varepsilon}\ {\rm d}\varepsilon
$$

So we find for the density of states:

$$
g(\varepsilon)=\frac{ {\rm d}N}{ {\rm d}\varepsilon}=\frac{Vm^{3/2}\sqrt{2\varepsilon}}{\pi^2\hbar^3}\propto\sqrt{\varepsilon}
$$

```python
E = np.linspace(0, 2, 500)
fig, ax = pyplot.subplots()

ax.plot(E, np.sqrt(E))

ax.set_ylabel(r"$g(\varepsilon)$")
ax.set_xlabel(r"$\varepsilon$")
draw_classic_axes(ax, xlabeloffset=.2)
```

Similarly,

- For 1D: $g(\varepsilon) = \frac{2 L}{\pi} \frac{ {\rm d}k}{ {\rm d}\varepsilon} \propto 1/\sqrt{\varepsilon}$
- For 2D: $g(\varepsilon) = \frac{k L^2}{\pi} \frac{ {\rm d}k}{ {\rm d}\varepsilon} \propto \text{constant}$


## Fermi energy, Fermi wavevector, Fermi wavelength

At $T=0$, the total number of electrons can be found by integrating the density of states up to the _Fermi energy_ $\varepsilon_{\rm F}$:

$$
N=\int_0^{\varepsilon_{\rm F}}g(\varepsilon){\rm d}\varepsilon \overset{\mathrm{3D}}{=} \frac{V}{3\pi^2\hbar^3}(2m\varepsilon_{\rm F})^{3/2}.
$$

Solving for the Fermi energy, we find
$$
\varepsilon_{\rm F}=\frac{\hbar^2}{2m}\left( 3\pi^2\frac{N}{V} \right)^{2/3}\equiv \frac{\hbar^2 k_{\rm F}^2}{2m},\
k_{\rm F}=\left( 3\pi^2\frac{N}{V} \right)^{1/3}.
$$

The quantity $k_{\rm F}=\frac{2\pi}{\lambda_{\rm F}}$ is called the _Fermi wavevector_, where $\lambda_{\rm F}$ is the _Fermi wavelength_, which is typically in the order of the atomic spacing.

For copper, the Fermi energy is ~7 eV. It would take a temperature of $\sim 70 000$K for electrons to gain such energy through a thermal excitation! The _Fermi velocity_ $v_{\rm F}=\frac{\hbar k_{\rm F}}{m}\approx$ 1750 km/s $\rightarrow$ electrons run with a significant fraction of the speed of light, only because lower energy states are already filled by other electrons.

The total number of electrons can be expressed as $N=\frac{2}{3}\varepsilon_{\rm F}g(\varepsilon_{\rm F})$.

```python
kf = 3.0;
extrapol = 4.0/3.0;
kfilled = np.linspace(-kf, kf, 500);
kstates = np.linspace(-extrapol*kf, extrapol*kf, 500);

Efilled = kfilled**2;
Estates = kstates**2;

fig = pyplot.figure();
ax = fig.add_subplot(111);
ax.fill_between(kfilled, Efilled, kf*kf, alpha=0.5);
ax.plot([kf, kf], [0.0, kf*kf], 'k:');
ax.plot(kstates, Estates, 'k--');
ax.plot(kfilled, Efilled, linewidth=4);
ax.axhline(kf*kf, linestyle="dotted", color='k');

ax.set_xticks([kf]);
ax.set_yticks([kf*kf + 0.4]);
ax.set_xticklabels([r"$k_F$"]);
ax.set_yticklabels([r"$\varepsilon_F$"]);

ax.set_xlabel(r"$k$");
ax.set_ylabel(r"$\varepsilon$");

ax.set_xlim(-kf*extrapol, kf*extrapol);
ax.set_ylim(0.0, kf*kf*extrapol);
draw_classic_axes(ax, xlabeloffset=.6);
```

The bold line represents all filled states at $T=0$. This is called the _Fermi sea_. 

New concept: _Fermi surface_ = all points in k-space with $\varepsilon=\varepsilon_{\rm F}$. For free electrons, the Fermi surface is a sphere.

$$
N=2\frac{\frac{4}{3}\pi k_{\rm F}^3}{\left( \frac{2\pi}{L} \right)^3}=\frac{k_{\rm F}^3V}{3\pi^2}\ \Rightarrow\ k_{\rm F}=\left( 3\pi^2\frac{N}{V} \right)^{1/3}
$$

![](figures/transport.svg)

The orange circle represents the Fermi surface at finite current $\rightarrow$ this circle will shift only slightly before the electrons reach terminal velocity $\rightarrow$ all transport takes place near the Fermi surface.


## Finite temperature, heat capacity

The probability for an electron to occupy a certain state depends on temperature, as described by the Fermi-Dirac distribution:

$$
f(\varepsilon,T)=\frac{1}{ {\rm e}^{(\varepsilon-\mu)/k_{\rm B}T}+1}
$$

```python
fig = pyplot.figure()
ax = fig.add_subplot(1,1,1)
xvals = np.linspace(0, 2, 200)
mu = .75
beta = 20
ax.plot(xvals, xvals < mu, ls='dashed', label='$T=0$')
ax.plot(xvals, 1/(np.exp(beta * (xvals-mu)) + 1),
        ls='solid', label='$T>0$')
ax.set_xlabel(r'$\varepsilon$')
ax.set_ylabel(r'$f(\varepsilon, T)$')
ax.set_yticks([0, 1])
ax.set_yticklabels(['$0$', '$1$'])
ax.set_xticks([mu])
ax.set_xticklabels([r'$\mu$'])
ax.set_ylim(-.1, 1.1)
ax.legend()
draw_classic_axes(ax)
pyplot.tight_layout()
```
where the chemical potential $\mu=\varepsilon_{\rm F}$ if $T=0$. Typically $\varepsilon_{\rm F}/k_{\rm B}$~70 000 K (~7 eV), whereas room temperature is only 300 K (~30 meV). Therefore, thermal smearing occurs only very close to the Fermi energy.

The total number of electrons $N$ is given by:

$$
N=\int_0^\infty f(\varepsilon,T)g(\varepsilon){\rm d}\varepsilon=\int_0^\infty n(\varepsilon,T){\rm d}\varepsilon
$$

We can use this to calculate the electronic contribution to the heat capacity.

```python
E = np.linspace(0, 2, 500)
fig, ax = pyplot.subplots()
ax.plot(E, np.sqrt(E), linestyle='dashed')
ax.text(1.7, 1.4, r'$g(\varepsilon)\propto \sqrt{\varepsilon}$', ha='center')
ax.fill_between(E, np.sqrt(E) * (E < 1), alpha=.3)

n = np.sqrt(E) / (1 + np.exp(20*(E-1)))
ax.plot(E, n)
ax.fill_between(E, n, alpha=.5)
w = .17
ax.annotate(s='', xy=(1, 1), xytext=(1-w, 1),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(1-w/2, 1.1, r'$\sim k_BT$', ha='center')
ax.plot([1-w, 1+w], [1, 0], c='k', linestyle='dashed')
ax.annotate(s='', xy=(1, 0), xytext=(1, 1),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(1.2, .7, r'$g(\varepsilon_F)$', ha='center')
ax.set_xticks([1])
ax.set_xticklabels([r'$\varepsilon_F$'])

ax.set_ylabel(r"$g(\varepsilon)$")
ax.set_xlabel(r"$\varepsilon$")
draw_classic_axes(ax, xlabeloffset=.2)
```

To estimate the heat capacity and derive its scaling with temperature, we use the triangle method depicted in the figure. A finite temperature causes electrons in the top triangle to be excited to the bottom triangle. Because the base of this triangle scales with $kT$ and its height with $ g(\varepsilon_{\rm F})$, it follows that the number of excited electrons $n_{\rm exc} \approx g(\varepsilon_{\rm F})k_{\rm B}T$ (neglecting pre-factors of order 1).

These electrons gain $kT$ of energy, so the total extra energy is 

$$
E(T)-E(0)=n_{\rm exc}k_{\rm B}T\approx g(\varepsilon_{\rm F})k_{\rm B}^2T^2
$$

so the heat capacity is given by

$$
C_{V,e}=\frac{ {\rm d}E}{ {\rm d}T} \approx 2 g(\varepsilon_{\rm F})k_{\rm B}^2T=\ ...\ =3 Nk_{\rm B}\frac{T}{T_{\rm F}}\propto T,
$$
where $T_{\rm F}=\frac{\varepsilon_{\rm F}}{k_{\rm B}}$ is the _Fermi temperature_.

How does $C_{V,e}$ relate to the phonon contribution $C_{V,p}$?

- At room temperature, $C_{V,p}=3Nk_{\rm B}\gg C_{V,e}$
- Near $T=0$, $C_{V,p}\propto T^3$ and $C_{V,e}\propto T$ $\rightarrow$ competition.

## Useful trick: scaling of $C_V$

Behavior of $C_V$ can be very quickly memorized or understood using the following mnemonic rule

> Particles with energy $E \leq kT$ are thermally excited, and each carries extra energy $kT$.

#### Example 1: electrons

$g(E_F)$ roughly constant ⇒ total energy in the thermal state is $T \times [T\times g(E_F)]$ ⇒ $C_V \propto T$.

#### Example 2: graphene with $E_F=0$ (midterm 2018)

$g(E) \propto E$ ⇒ total energy is $T \times T^2$ ⇒ $C_V \propto T^2$.

#### Example 3: phonons in 3D at low temperatures.

$g(E) \propto E^2$ ⇒ total energy is $T \times T^3$ ⇒ $C_V \propto T^3$.


## Exercises
#### Exercise 1: potassium
The Sommerfeld model provides a good description of free electrons in alkali metals such as potassium, which has a Fermi energy of 2.12 eV (data from Ashcroft, N. W. and Mermin, N. D., Solid State Physics, Saunders, 1976.).

1. Check the [Fermi surface database](fermi_surfaces.md) in the attic. Explain why potassium and (most) other alkali metals can be described well with the Sommerfeld model.
2. Calculate the Fermi temperature, Fermi wave vector and Fermi velocity for potassium.
3. Why is the Fermi temperature much higher than room temperature?
4. Calculate the free electron density in potassium.
5. Compare this with the actual electron density of potassium, which can be calculated by using the density, atomic mass and atomic number of potassium. What can you conclude from this?

#### Exercise 2: the $n$-dimensional free electron model.
In the lecture, it has been explained that the density of states (DOS) of the free electron model is proportional to $1/\sqrt{\epsilon}$ in 1D, constant in 2D and proportional to $\sqrt{\epsilon}$ in 3D. In this exercise, we are going to derive the DOS of the free electron model for an arbitrary number of dimensions.
Suppose we have an $n$-dimensional hypercube with length $L$ for each side and contains free electrons.

1. Assuming periodic boundary conditions, what is the distance between nearest-neighbour points in $\mathbf{k}$-space? What is the density of $\mathbf{k}$-points in n-dimensional $\mathbf{k}$-space?
2. The number of $\mathbf{k}$-points with a magnitude between $k$ and $k + dk$ is given by $g(k)dk$. Using the answer for 1, find $g(k)$ for 1D, 2D and 3D.
3. Now show that $g(k)$ for $n$ dimensions is given by
  $$g(k) = \frac{1}{\Gamma(n/2)} \left( \frac{L }{ \sqrt{\pi}} \right)^n \left( \frac{k}{2} \right)^{n-1},$$ where $\Gamma(z)$ is the [gamma function](https://en.wikipedia.org/wiki/Gamma_function).

	??? hint
		You will need the area of an $n$-dimensional sphere and this can be found on [Wikipedia](https://en.wikipedia.org/wiki/N-sphere#Volume_and_surface_area) (blue box on the right).

4. Check that this equation is consistent with your answers in 2. 

	??? hint
		Check [Wikipedia](https://en.wikipedia.org/wiki/Particular_values_of_the_gamma_function) to find out how to deal with half-integer values in the gamma function.

5. Using the expression in 3, calculate the DOS (do not forget the spin degeneracy).
6. Give an integral expression for the total number of electrons and for their total energy in terms of the DOS, the temperature $T$ and the chemical potential $\mu$ (_you do not have to work out these integrals_).
7. Work out these integrals for $T = 0$.

#### Exercise 3:  a hypothetical material
A hypothetical metal has a Fermi energy $\epsilon_F = 5.2 \, \mathrm{eV}$ and a DOS per unit volume $g(\epsilon) =  2 \times 10^{10} \, \mathrm{eV}^{-\frac{3}{2}} \sqrt{\epsilon}$.

1. Give an integral expression for the total energy of the electrons in this hypothetical material in terms of the DOS $g(\epsilon)$, the temperature $T$ and the chemical potential $\mu = \epsilon_F$.
2. Find the ground state energy at $T = 0$.
3. In order to obtain a good approximation of the integral for non-zero $T$, one can make use of the [Sommerfeld expansion](https://en.wikipedia.org/wiki/Sommerfeld_expansion). Using this expansion, find the difference between the total energy of the electrons for $T = 1000 \, \mathrm{K}$ with that of the ground state.
4. Now, find this difference in energy by calculating the integral found in 1 numerically. Compare your result with 3. 

	??? hint
		 You can do numerical integration in MATLAB with [`integral(fun,xmin,xmax)`](https://www.mathworks.com/help/matlab/ref/integral.html).

5. Calculate the heat capacity for $T = 1000 \, \mathrm{K}$ in eV/K.
6. Numerically compute the heat capacity by approximating the derivative of energy difference found in 4 with respect to $T$. To this end, make use of the fact that $$\frac{dy}{dx}=\lim_{\Delta x \to 0} \frac{y(x + \Delta x) - y(x - \Delta x)}{2 \Delta x}.$$ Compare your result with 5.

#### Exercise 4: graphene
One of the most famous recently discovered materials is [graphene](https://en.wikipedia.org/wiki/Graphene), which consists of carbon atoms arranged in a 2D honeycomb structure. In this exercise, we will focus on the electrons in bulk graphene. Unlike in metals, electrons in graphene cannot be treated as 'free'. However, close to the Fermi level, the dispersion relation can be approximated by a linear relation:
$$ \epsilon(\mathbf{k}) = \pm c|\mathbf{k}|.$$ Note that the $\pm$ here means that there are two energy levels at a specified $\mathbf{k}$. The Fermi level is set at $\epsilon_F = 0$.

1. Make a sketch of the dispersion relation. What other well-known particles have a linear dispersion relation?
2. Using the dispersion relation and assuming periodic boundary conditions, derive an expression for the DOS of graphene. Your result should be linear with $|\epsilon|$. Do not forget spin degeneracy, and take into account that graphene has an additional two-fold 'valley degeneracy'.
3. At finite temperatures, assume that electrons close to the Fermi level (i.e. not more than $k_B T$ below the Fermi level) will get thermally excited, thereby increasing their energy by $k_B T$. Calculate the difference between the energy of the thermally excited state and that of the ground state $E(T)-E_0$. To do so, show first that the number of electrons that will get excited is given by $$n_{ex} = \frac{1}{2} g(-k_B T) k_B T.$$
4. Calculate the heat capacity $C_V$ as a function of the temperature $T$.
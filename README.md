# Solid state lectures

Lecture notes and teaching material used for the Delft University of Technology course Solid State Physics (Vaste Stof Fysica, TN2844).

The compiled materials are available at http://solidstate.quantumtinkerer.tudelft.nl

